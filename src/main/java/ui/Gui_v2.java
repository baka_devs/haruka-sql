package ui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import ui.WhereAmI;
import core.Main;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Vector;

public class Gui_v2 {
public static boolean init;
	private static Gui_v2 ui = null;
	private JFrame frame;
	public static JTextArea ConsoleText;
	private JTextField other;
	private JTable SQLtable;
	private static ArrayList<String> chartChatIds = new ArrayList<>();
	public static Gui_v2 getUI() {
		if(ui==null) {
			guimain();
		}
		return ui;
	}
	private static void guimain() {
		EventQueue.invokeLater(() -> {
            try {
                ui = new Gui_v2();
				ui.frame.setVisible(true);
				Main.Output("ui: Initializing done!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
	}
	public Gui_v2() throws SQLException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		initialize();
	}

	private void initialize() throws SQLException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		System.setProperty("awt.useSystemAAFontSettings", "on");
		System.setProperty("swing.aatext", "true");
		frame = new JFrame("NKVD sql Browser v" + Main.version + " ui");
		frame.setBounds(100, 100, 1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);


		JPanel Console = new JPanel();
		tabbedPane.addTab("Console", null, Console, null);
		SpringLayout sl_Console = new SpringLayout();
		Console.setLayout(sl_Console);

		JScrollPane ConsoleScrollpane = new JScrollPane();
		sl_Console.putConstraint(SpringLayout.NORTH, ConsoleScrollpane, 0, SpringLayout.NORTH, Console);
		sl_Console.putConstraint(SpringLayout.WEST, ConsoleScrollpane, 0, SpringLayout.WEST, Console);
		sl_Console.putConstraint(SpringLayout.SOUTH, ConsoleScrollpane, 0, SpringLayout.SOUTH, Console);
		sl_Console.putConstraint(SpringLayout.EAST, ConsoleScrollpane, 0, SpringLayout.EAST, Console);
		Console.add(ConsoleScrollpane);

		ConsoleText = new JTextArea();
		ConsoleScrollpane.setViewportView(ConsoleText);

		JPanel SQLbrowser = new JPanel();
		tabbedPane.addTab("sql browser", null, SQLbrowser, null);
		SpringLayout sl_SQLbrowser = new SpringLayout();
		SQLbrowser.setLayout(sl_SQLbrowser);

		Choice Username = new Choice();
		Username.add("any Username");
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, Username, 12, SpringLayout.NORTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, Username, 12, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, Username, 112, SpringLayout.WEST, SQLbrowser);
		SQLbrowser.add(Username);

		Choice UserId = new Choice();
		UserId.add("any UserId");
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, UserId, 12, SpringLayout.NORTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, UserId, 124, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, UserId, 224, SpringLayout.WEST, SQLbrowser);
		SQLbrowser.add(UserId);

		Choice Chatname = new Choice();
		Chatname.add("any Chatname");
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, Chatname, 12, SpringLayout.NORTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, Chatname, 236, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, Chatname, 336, SpringLayout.WEST, SQLbrowser);
		SQLbrowser.add(Chatname);

		Choice ChatId = new Choice();
		ChatId.add("any ChatId");
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, ChatId, 12, SpringLayout.NORTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, ChatId, 348, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, ChatId, 448, SpringLayout.WEST, SQLbrowser);
		SQLbrowser.add(ChatId);

		other = new JTextField();
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, other, 0, SpringLayout.NORTH, Username);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, other, 12, SpringLayout.EAST, ChatId);
		sl_SQLbrowser.putConstraint(SpringLayout.SOUTH, other, 33, SpringLayout.NORTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, other, -87, SpringLayout.EAST, SQLbrowser);
		SQLbrowser.add(other);
		other.setColumns(10);

		JSeparator SQLbrowserSseparator = new JSeparator();
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, SQLbrowserSseparator, 6, SpringLayout.SOUTH, Username);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, SQLbrowserSseparator, 0, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.SOUTH, SQLbrowserSseparator, 0, SpringLayout.SOUTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, SQLbrowserSseparator, 0, SpringLayout.EAST, SQLbrowser);
		SQLbrowser.add(SQLbrowserSseparator);

		JScrollPane SQLbrowserScrollPane = new JScrollPane();
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, SQLbrowserScrollPane, 0, SpringLayout.NORTH, SQLbrowserSseparator);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, SQLbrowserScrollPane, 0, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.SOUTH, SQLbrowserScrollPane, 0, SpringLayout.SOUTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, SQLbrowserScrollPane, 0, SpringLayout.EAST, SQLbrowser);
		SQLtable = new JTable();
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, SQLtable, 128, SpringLayout.SOUTH, SQLbrowserSseparator);
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, SQLtable, 0, SpringLayout.WEST, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.SOUTH, SQLtable, -9, SpringLayout.SOUTH, SQLbrowser);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, SQLtable, 0, SpringLayout.EAST, SQLbrowserSseparator);
		SQLbrowser.add(SQLbrowserScrollPane);
		JButton Exec = new JButton("Exec");
		sl_SQLbrowser.putConstraint(SpringLayout.WEST, Exec, 6, SpringLayout.EAST, other);
		sl_SQLbrowser.putConstraint(SpringLayout.EAST, Exec, -1, SpringLayout.EAST, SQLbrowser);
		Exec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					SQLbrowser(SQLtable, ChatId, UserId, Username, Chatname, other, SQLbrowserScrollPane, SQLbrowser);
				} catch (SQLException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		sl_SQLbrowser.putConstraint(SpringLayout.NORTH, Exec, 0, SpringLayout.NORTH, Username);
		sl_SQLbrowser.putConstraint(SpringLayout.SOUTH, Exec, 0, SpringLayout.SOUTH, Username);
		SQLbrowser.add(Exec);

		JPanel Other = new JPanel();
		tabbedPane.addTab("Other", null, Other, null);
		Other.setLayout(null);

		Choice ChartsChatName = new Choice();
		ChartsChatName.setBounds(497, 29, 351, 20);
		Other.add(ChartsChatName);


		Choice ChartsUsername = new Choice();
		ChartsUsername.setBounds(54, 29, 351, 20);
		ChartsUsername.setVisible(false);
		Other.add(ChartsUsername);


		JLabel lblCharts = new JLabel("Charts");
		lblCharts.setBounds(1, 29, 47, 20);
		Other.add(lblCharts);
		JCheckBox chckbxByUser = new JCheckBox("by user");
		JButton BuildChart = new JButton("Build chart");
		BuildChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxByUser.isSelected() == true) {
					long UserId = WhereAmI.UserIds.get(ChartsUsername.getSelectedIndex());
					BuildChart(0, UserId);
				} else {
					for (int i = 0; i < WhereAmI.ChatIds.size(); i++) {
						Statement stm;
						try {
							stm = Main.getConnection().createStatement();
							String query = "SELECT chat_name, chat_id, user_name FROM aka_log WHERE chat_id=";
							query += WhereAmI.ChatIds.get(i);
							query += " ORDER BY time_stamp DESC";
							Main.Output(String.valueOf("Chart ui: Executing:" + query));
							ResultSet rs = stm.executeQuery(query);
							stm = Main.getConnection().createStatement();
							rs.first();
							String chk = rs.getString("chat_name") + " (" + rs.getString("chat_id") + ")";
							if (chk.equals(ChartsChatName.getSelectedItem())) {
								BuildChart(rs.getLong("chat_id"), 0);
							}
						} catch (SQLException | InterruptedException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		BuildChart.setBounds(873, 29, 130, 20);
		Other.add(BuildChart);

		JSeparator OtherSeparator1 = new JSeparator();
		OtherSeparator1.setBounds(0, 55, 1003, 3);
		Other.add(OtherSeparator1);

		JLabel lblBotcontrol = new JLabel("Bot Control");
		lblBotcontrol.setBounds(12, 70, 108, 15);
		Other.add(lblBotcontrol);

		JButton btnNewButton_1 = new JButton("Exit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_1.setBounds(10, 97, 66, 15);
		Other.add(btnNewButton_1);

		JButton btnRestart = new JButton("Restart");
		btnRestart.setBounds(88, 97, 84, 15);
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Main.restart();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		Other.add(btnRestart);

		JSeparator OtherSeparator2 = new JSeparator();
		OtherSeparator2.setBounds(0, 124, 1003, 2);
		Other.add(OtherSeparator2);
		
				JLabel NKVDchan = new JLabel("");
				NKVDchan.setBounds(810, 132, 219, 690);
				NKVDchan.setIcon(new ImageIcon(getClass().getResource("/img.png")));
				Other.add(NKVDchan);

		JLabel lblAbout = new JLabel("About");
		lblAbout.setBounds(10, 138, 66, 15);
		Other.add(lblAbout);
		Main.Output("ui: Initializing...");
		while (true) {
			if (Main.init) {
				for (int i = 0; i < WhereAmI.ChatIds.size(); i++) {
					if(!chartChatIds.contains(WhereAmI.ChatIds.get(i).toString())) {
					ChartsChatName.add(WhereAmI.ChatNames.get(i) + " (" + WhereAmI.ChatIds.get(i) + ")");
					System.out.println("Added: "+WhereAmI.ChatNames.get(i) + " (" + WhereAmI.ChatIds.get(i) + ")");
					chartChatIds.add(WhereAmI.ChatIds.get(i).toString());
					}
				}
				for (int i = 0; i < WhereAmI.Usernames.size(); i++) {
					if (WhereAmI.Usernames.get(i) != null) {
						Username.add(WhereAmI.Usernames.get(i));
					}
				}
				for (int i = 0; i < WhereAmI.UserIds.size(); i++) {
					UserId.add(String.valueOf(WhereAmI.UserIds.get(i)));
				}
				for (int i = 0; i < WhereAmI.ChatNames.size(); i++) {
					if (WhereAmI.ChatNames.get(i) != null) {
						Chatname.add(WhereAmI.ChatNames.get(i));
					}
				}
				init = true;
				break;
			}
			Thread.sleep(100);

		}
				ChartsInit(ChartsChatName, ChartsUsername);
				
						
						chckbxByUser.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (ChartsUsername.isVisible() == true) {
									ChartsUsername.setVisible(false);
									ChartsChatName.setVisible(true);
								} else {
									ChartsUsername.setVisible(true);
									ChartsChatName.setVisible(false);
								}
							}
						});
						chckbxByUser.setBounds(411, 29, 100, 23);
						Other.add(chckbxByUser);
				
				JTextPane txtpnNkvdSqlBrowser = new JTextPane();
				txtpnNkvdSqlBrowser.setBackground(UIManager.getColor("Button.background"));
				txtpnNkvdSqlBrowser.setFont(new Font("Tahoma", Font.PLAIN, 38));
				txtpnNkvdSqlBrowser.setText("NKVD\r\nsql Browser\r\nv."+Main.version);
				txtpnNkvdSqlBrowser.setBounds(10, 164, 228, 370);
				Other.add(txtpnNkvdSqlBrowser);
				
				JLabel NKVD = new JLabel("");
				NKVD.setIcon(new ImageIcon(getClass().getResource("/nkvd.png")));
				NKVD.setBounds(368, 162, 250, 453);
				Other.add(NKVD);
				frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

	}
public void SQLbrowser(JTable table,Choice ChatId,Choice UserId,Choice Username, Choice Chatname,JTextField other,JScrollPane SQLbrowserScrollPane, JPanel SQLbrowser ) throws SQLException, InterruptedException{
		String query = ("SELECT * FROM aka_log WHERE ");
        boolean bChatId;
        boolean bUserId;
        boolean bUsername;
		if (Objects.equals(ChatId.getSelectedItem(), "any ChatId")) {bChatId=false;} else {
            bChatId=true;
            query+=("chat_id='"+ChatId.getSelectedItem()+"' ");
        }
		if (Objects.equals(UserId.getSelectedItem(), "any UserId")) {bUserId=false;} else {
            bUserId=true;
            if(!bChatId)
            {
            query+=("user_id='"+UserId.getSelectedItem()+"'");
            }
            else
            {
                query+=("AND user_id='"+UserId.getSelectedItem()+"'");
            }
        }
		if (Username.getSelectedItem() == "any Username") bUsername = false;
		else {
            bUsername=true;
            if(bChatId==false&&bUserId==false)
            {
            query+=("user_name='"+Username.getSelectedItem()+"'");
            }
            else
            {
                query+=("AND user_name='"+Username.getSelectedItem()+"'");
            }
        }
		if(Chatname.getSelectedItem()!="any Chatname")
        {
            if(bChatId==false&&bUserId==false&&bUsername==false)
            {
            query+=("chat_name='"+Chatname.getSelectedItem()+"'");
            }
            else
            {
                query+=("AND chat_name='"+Chatname.getSelectedItem()+"'");
            }
        }
            Main.Output("sql browser: Executing: "+query);
            Statement stm;
            try {
                query+="ORDER BY time_stamp";
                stm = Main.getConnection().createStatement();
                ResultSet rs = stm.executeQuery(query);
                java.sql.ResultSetMetaData metaData = rs.getMetaData();
        	    Vector<String> columnNames = new Vector<String>();
        	    int columnCount = metaData.getColumnCount();
        	    for (int column = 1; column <= columnCount; column++) {
        	        columnNames.add(metaData.getColumnName(column));
        	    }
        	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        	    while (rs.next()) {
        	        Vector<Object> vector = new Vector<Object>();
        	        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
        	            vector.add(rs.getObject(columnIndex));
        	        }
        	        data.add(vector);
        			SQLbrowserScrollPane.setViewportView(SQLtable);
        	        SQLtable.setModel(new DefaultTableModel(data, columnNames));
            }
            }catch (SQLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
}
public void ChartsInit(Choice ChartsChatName, Choice ChartsChatUsername)
{
	for(int i=0;i<WhereAmI.ChatIds.size();i++)
    {
  	Statement stm;
  	try {
			if(!WhereAmI.UserIds.contains(WhereAmI.ChatIds.get(i)))
			{
			stm = Main.getConnection().createStatement();
			String query = "SELECT chat_name, chat_id FROM aka_log WHERE chat_id=";
			query+=WhereAmI.ChatIds.get(i);
			query+=" ORDER BY time_stamp;";
			Main.Output(String.valueOf("Chart ui: Executing:" + query));
			ResultSet rs = stm.executeQuery(query);
			stm = Main.getConnection().createStatement();
			rs.first();
			if(!chartChatIds.contains(rs.getString("chat_id"))) {
			ChartsChatName.add(rs.getString("chat_name")+" ("+rs.getString("chat_id")+")");
			chartChatIds.add(rs.getString("chat_id"));
			}
			}
		} catch (SQLException | InterruptedException e1) {
			e1.printStackTrace();
		}
    }
  	for(int i=0;i<WhereAmI.UserIds.size();i++)
    {
  	try {
  		Statement stm;
			stm = Main.getConnection().createStatement();
			String query = "SELECT user_name, user_id FROM aka_log WHERE user_id=";
			query+=WhereAmI.UserIds.get(i);
			//Main.Main.Output(String.valueOf("Chart ui: Executing:" + query));
			ResultSet rs = stm.executeQuery(query);
			stm = Main.getConnection().createStatement();
			rs.first();
			ChartsChatUsername.add(rs.getString("user_name")+" ("+rs.getString("user_name")+")");
		} catch (SQLException | InterruptedException e1) {
			e1.printStackTrace();
		}
    }
}
public static void BuildChart(long ChatId, long UserId)
{
	String ChartName = null;
	int count = 0;
	DefaultPieDataset pieDataset = new DefaultPieDataset();
	if(ChatId!=0)
	{
		for(int i = 0;i<WhereAmI.UserIds.size();i++)
		{
			Statement stm;
			try {
				stm = Main.getConnection().createStatement();
				String query = "SELECT count(*), chat_name FROM aka_log WHERE user_id=";
				query+=WhereAmI.UserIds.get(i);
				query+=" AND chat_id ="+String.valueOf(ChatId)+" ORDER BY time_stamp ASC";
				Main.Output("Chart ui: Executing:" + query);
				ResultSet rs = stm.executeQuery(query);
				rs.first();
				if((rs.getInt("count(*)")!=0&&WhereAmI.Usernames.get(i)!=null)&&(rs.getInt("count(*)")>50))
				{
				count = rs.getInt("count(*)");
				pieDataset.setValue(WhereAmI.Usernames.get(i), count);
				}
				else if(WhereAmI.Usernames.get(i)==null&&(rs.getInt("count(*)")>50)){
					pieDataset.setValue(WhereAmI.UserIds.get(i), count);
				}
				if(WhereAmI.ChatNames.get(WhereAmI.ChatIds.indexOf(ChatId))!= null)
				{
		        ChartName = WhereAmI.ChatNames.get(WhereAmI.ChatIds.indexOf(ChatId));
				}
		        else 
		        {
			    ChartName = String.valueOf(ChatId);
		}
			} catch (SQLException | InterruptedException e2) {
				Main.Output("EXCEPTION");
				e2.printStackTrace();
			}
		}
	}
	else
	{
		for(int i = 0;i<WhereAmI.ChatIds.size();i++)
		{
			Statement stm;
			try {
				stm = Main.getConnection().createStatement();
				String query = "SELECT count(*), chat_name, chat_id, user_name FROM aka_log WHERE user_id=";
				query+=UserId;
				query+=" AND chat_id=" +WhereAmI.ChatIds.get(i)+" ORDER BY time_stamp DESC";
				//Main.Main.Output(String.valueOf("Chart ui: Executing:" + query));
				ResultSet rs = stm.executeQuery(query);
				rs.first();
				if((rs.getInt("count(*)")!=0)&&(rs.getInt("count(*)")>10)&&WhereAmI.ChatIds.get(i)!=null)
				{
				count = rs.getInt("count(*)");
				pieDataset.setValue(WhereAmI.ChatNames.get(i), count);
				}
				else if(WhereAmI.ChatIds.get(i)==null&&(rs.getInt("count(*)")>10)){
					pieDataset.setValue(WhereAmI.ChatIds.get(i), count);
				}
				if(WhereAmI.Usernames.get(WhereAmI.UserIds.indexOf(UserId))!= null)
						{
				ChartName = WhereAmI.Usernames.get(WhereAmI.UserIds.indexOf(UserId));
						}
				else {
					ChartName = String.valueOf(UserId);
				}
			} catch (SQLException | InterruptedException e2) {
				Main.Output("EXCEPTION");
				e2.printStackTrace();
			}
		}
	}
	pieDataset.sortByValues(org.jfree.chart.util.SortOrder.DESCENDING);
	JFreeChart chart = ChartFactory.createPieChart(ChartName,pieDataset, true,true,true);
	ChartFrame chrfrm = new ChartFrame("Chart", chart);
	chrfrm.setVisible(true);
	chrfrm.setResizable(false);
	chrfrm.setSize(800, 600);
}
}