package ui;

import core.Main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class WhereAmI{
	static int count = 0;
	public static ArrayList<Long> ChatIds = new ArrayList<>();
	public static ArrayList<Long> UserIds = new ArrayList<>();
	public static ArrayList<String> Usernames = new ArrayList<>();
	public static ArrayList<String> ChatNames = new ArrayList<>();
public static void init() throws InterruptedException{
{
	String query = ("SELECT DISTINCT user_name, user_id FROM aka_log;");
	try{
		Main.Output("Initializing...");
		Main.Output("Getting users...");
		Statement stm = Main.getConnection().createStatement();
		Statement stm1 = Main.getConnection().createStatement();
		Main.Output("Executing:"+query);
		ResultSet rs = stm.executeQuery(query);
		count=0;
		while(rs.next())
		{
			if(!UserIds.contains(rs.getLong("user_id")))
			{
				if(!(rs.getLong("user_id")==0L)) {
					Usernames.add(rs.getString("user_name"));
					UserIds.add(rs.getLong("user_id"));
					count++;
				} else {
					Usernames.add("Other users");
					UserIds.add(rs.getLong("user_id"));
					count++;
				}		
			} else if((rs.getLong("user_id")==0L)) {
				count++;
			}
            
		}
		Main.Output("Got "+count+" users.");

		Main.Output("Getting Chats...");
		query = ("SELECT DISTINCT chat_id, chat_name FROM aka_log ORDER BY time_stamp DESC;");
		Main.Output("Executing:"+query);
		rs = stm.executeQuery(query);
		count=0;
		while(rs.next())
		{
			if(!ChatIds.contains(rs.getLong("chat_id")))
			{
			ChatIds.add(rs.getLong("chat_id"));
			count++;
			if(rs.getString("chat_name")!=null)
			{
				ChatNames.add(rs.getString("chat_name"));
			}
			else
			{
				Main.Output("SELECT user_name FROM aka_log WHERE user_id="+rs.getLong("chat_id"));
				ResultSet rs1 = stm1.executeQuery("SELECT user_name FROM aka_log WHERE user_id="+rs.getLong("chat_id"));
				rs1.next();
				count--;
				ChatNames.add(rs1.getString("user_name"));
				rs1.close();
			}
			}
		}
		Main.Output("Got "+count+" chats.");
		rs.close();
		} catch (SQLException e) {
		    e.printStackTrace();
		}
		}
}
}
