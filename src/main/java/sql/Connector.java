package sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import core.Main;
import com.mysql.cj.jdbc.MysqlDataSource;
public class Connector {
	private String URL      ="";
    private String USER     = "";
    private String PASSWORD = "";
    private String DBNAME = "";
	    private static Connection connection = null;
	    Connector() {
	        try {
				init();
	        	MysqlDataSource dataSource = new MysqlDataSource();
	        	dataSource.setUser(USER);
	        	dataSource.setPassword(PASSWORD);
	        	dataSource.setServerName(URL);
	        	dataSource.setDatabaseName(DBNAME);
	        	dataSource.setAutoReconnect(true);
	        	dataSource.setAutoReconnectForPools(true);
	        	dataSource.setReconnectAtTxEnd(true);
	        	dataSource.setCharacterEncoding("utf8");
	        	connection = dataSource.getConnection();
	        	Statement st = connection.createStatement();
	        	st.executeQuery("SET NAMES utf8mb4");
	        } catch (SQLException e) {
	        	Main.Output("Could not establish sql connection. Exiting...");
	            e.printStackTrace();
	            System.exit(0);
	        }
	    }
	   public static synchronized Connection getConnection() {
		   if(connection!=null) {
	        return connection;
		   } else {
			   new Connector();
			   return connection;
		   }
	    }
	   private void init(){
	        File sqlfile = new File(new File(".").getAbsolutePath()+"/config/sql");
	        Main.Output("sql config file is in: " + sqlfile.getAbsolutePath());
	        try {
	            BufferedReader reader = new BufferedReader(new FileReader(sqlfile));
	            this.URL = reader.readLine().trim();
	            this.USER = reader.readLine().trim();
	            this.PASSWORD = reader.readLine().trim();
	            this.DBNAME = reader.readLine().trim();
	            reader.close();
	        } catch (IOException e) {
	        	Main.Output("There is no sql config file or the file is corrupt! Create one on the path above manually. Exiting...");
	            System.exit(0);
	        }
	   }
}
