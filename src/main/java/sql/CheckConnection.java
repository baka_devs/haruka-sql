package sql;

import java.sql.Connection;
import java.sql.SQLException;

public class CheckConnection {
	private static Connector connec = new Connector();
	private static Connection conn = connec.getConnection();
public static Connection check() throws SQLException
{
	if(conn!=null&& !conn.isClosed())
	{
	return conn;
	}
	else
	{
	connec = new Connector();
	conn = connec.getConnection();
	return null;
	}
}
}
