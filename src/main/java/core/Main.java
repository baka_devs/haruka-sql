package core;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


import sql.CheckConnection;
import ui.Gui_v2;
import ui.WhereAmI;


public class Main {
	public static boolean init ;
	public static String version = "0.0.3(ver.3)";
	public static void main(String args[]) throws InterruptedException{
		init = true;
		WhereAmI.init();
		Gui_v2.getUI();
	}
	public static void Output(String inptext)
	{
		System.out.println(inptext+"\n");
		if(Gui_v2.ConsoleText!=null)
		Gui_v2.ConsoleText.append(inptext+"\n");
	}
	public static Connection getConnection() throws InterruptedException, SQLException
	{
		while(CheckConnection.check()==null)
		{
			Main.Output("No sql connection... Retrying in 1 sec.");
			Thread.sleep(1000);
		}
		return CheckConnection.check();
	}
	public static void restart() throws IOException
	{
			Runtime.getRuntime().exec("java -jar NKVD_SQLBrowser."+version+".jar");
			System.exit(0);
	}
}
